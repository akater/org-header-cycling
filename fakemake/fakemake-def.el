;;;  -*- lexical-binding: t -*-

(defconst-with-prefix fakemake
  feature 'org-header-cycling
  authors "Dima Akater"
  first-publication-year-as-string "2020"
  org-files-in-order '("org-header-cycling-core"
                       "org-header-cycling")
  site-lisp-config-prefix "50"
  license "GPL-3")
